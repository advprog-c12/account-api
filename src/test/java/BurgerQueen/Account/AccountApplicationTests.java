package BurgerQueen.Account;

import BurgerQueen.Account.AccountApplication;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testAccount() throws Exception {
        mockMvc.perform(get("/sign-in"))
                .andExpect(content().string(containsString("AccountTest")));
    }

    @Test
    public void testCallSingleAcc() throws Exception {
        mockMvc.perform(get("/abrahamcc@gmail.com"))
                .andExpect(content().string(containsString("{\"email\":\"abrahamcc@gmail.com\",\"name\":\"bram\",\"address\":\"Sisesa House\"}")));
    }

    @Test
    public void testGetAllAcc() throws Exception {
        mockMvc.perform(get("/all"))
                .andExpect(content().string(containsString("[{\"email\":\"palitocc@gmail.com\",\"name\":\"palito\",\"address\":\"Tulip House\"},{\"email\":\"abrahamcc@gmail.com\",\"name\":\"bram\",\"address\":\"Sisesa House\"}]")));
    }

    @Test
    public void testHasMain() throws IOException {
        AccountApplication.main(new String[]{ "--spring.main.web-environment=false",});

    }
}
