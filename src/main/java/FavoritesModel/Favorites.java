package FavoritesModel;

import javax.persistence.*;
import AccountModel.Account;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "favorites")
public class Favorites {

    @Id
    @Column(name = "id")
    private String id;

    @ManyToMany(mappedBy = "favoritesSet")
    private List<Account> accountSet = new ArrayList<Account>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Account> getAccountSet() {
        return accountSet;
    }

    public void setAccountSet(List<Account> accountSet) {
        this.accountSet = accountSet;
    }
}
