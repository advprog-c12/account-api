package Repository;

import FavoritesModel.Favorites;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoritesRepository extends JpaRepository<Favorites, Long>{
    Favorites addFavorites(String id, String accountId);
    List<Favorites> findAllFavoritesByAccountId(String accountId);
    void deleteFavoritesByIdAndAccountId(String id, String accountId);
}
