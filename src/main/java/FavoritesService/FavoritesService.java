package FavoritesService;

import FavoritesModel.Favorites;

public interface FavoritesService {
    Favorites addFavorites(String id, String accountId);
    String getAllFavorites(String accountId);
    Boolean deleteFavorites(String id, String accountId);
}
