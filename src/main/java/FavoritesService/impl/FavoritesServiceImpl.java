package FavoritesService.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import java.util.List;

import FavoritesModel.Favorites;
import Repository.FavoritesRepository;
import FavoritesService.FavoritesService;

import javax.transaction.Transactional;

@Service
@Transactional
public class FavoritesServiceImpl implements FavoritesService {

    @Autowired
    private FavoritesRepository favoritesRepository;

//    public ArrayList<Favorites> addFavorites(String akun, String burger) {
//        Favorites newFav = new Favorites();
//        newFav.setIdAkun(akun);
//        newFav.setIdBurger(burger);
//        favorites.add(newFav);
//        return favorites;
//    }

    @Override
    public Favorites addFavorites(String accountId, String id) {
        Favorites favorites = new Favorites();
        favorites.setId(id);
        return favoritesRepository.save(favorites);
    }

    @Override
    public String getAllFavorites(String accountId) {
        List<Favorites> favorites = (List<Favorites>) favoritesRepository.findAllFavoritesByAccountId(accountId);
        String favorites_json = new Gson().toJson(favorites);
        System.out.println(favorites_json);
        return favorites_json;
    }

    @Override
    public Boolean deleteFavorites(String accountId, String id) {
        Boolean isSuccess = true;

        try {
            favoritesRepository.deleteFavoritesByIdAndAccountId(id, accountId);
        } catch (Exception e) {
            isSuccess = false;
        }

        return isSuccess;
    }
}
