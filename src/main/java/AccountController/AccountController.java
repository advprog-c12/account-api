package AccountController;

import java.util.ArrayList;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import AccountModel.Account;
import AccountService.AccountService;

@CrossOrigin
@RestController
@RequestMapping("")
public class AccountController {

    @Autowired
    AccountService accountService;

    @RequestMapping("/all")
    public ArrayList<Account> getAll() {
        return accountService.getAll();
    }

    @RequestMapping("{email}")
    public Account getAccount(@PathVariable("email") String email) {
        return accountService.getAccount(email);
    }

    @GetMapping("/sign-in")
    public String status() {
        return "AccountTest";
    }

    @GetMapping("/getAcc/{email}")
    public String getOrder(@PathVariable String email) {
        String acc_json = accountService.getAccByEmail(email);
        return acc_json;
    }

    @GetMapping("/addAcc/{email}")
    public String addAcc(@PathVariable String email) {
        Account account = new Account(email);
        accountService.addAccount(account);
        return "success";
    }

    @GetMapping("/getAllAccounts")
    public String getAllAccounts() {
        String account_json = accountService.findAll();
        return account_json;
    }

    @GetMapping("/editAcc/{email}/{name}/{address}")
    public String addAcc(@PathVariable String email, String name, String address) {
        Account accedit = accountService.getAccount(email);
        accedit.setAddress(address);
        accedit.setName(name);
        return "success";
    }


}