package FavoritesController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import FavoritesModel.Favorites;
import FavoritesService.impl.FavoritesServiceImpl;

@CrossOrigin
@RestController
public class FavoritesController {

    @Autowired
    FavoritesServiceImpl favoritesService;

    @GetMapping("/favorites/{id_akun}")
    public String getAllFavorites(@PathVariable String id_akun){
        String result = favoritesService.getAllFavorites(id_akun);
        return result;
    }

    @RequestMapping("/add-favorites/{id_akun}/{id_burger}")
    public String addFavorites(@PathVariable("accountId") String id_akun, @PathVariable("id") String id_burger) {
        favoritesService.addFavorites(id_akun, id_burger);
        return "success";
    }

    @RequestMapping("/delete-favorites/{id_akun}/{id_burger}")
    public String deleteFavorites(@PathVariable("accountId") String id_akun, @PathVariable("id") String id_burger) {
        favoritesService.deleteFavorites(id_akun, id_burger);
        return "success";
    }
}
