package AccountService;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import AccountModel.Account;
import AccountRepository.AccountRepository;


import com.google.gson.Gson;
import com.google.gson.JsonObject;

import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AccountService {

    @Autowired
    private AccountRepository repository;

    ArrayList<Account> accounts = new ArrayList<Account>();


    public AccountService() {
        Account p = new Account("palitocc@gmail.com");
        p.setAddress("Tulip House");
        p.setName("palito");
        accounts.add(p);

        p = new Account("abrahamcc@gmail.com");
        p.setAddress("Sisesa House");
        p.setName("bram");
        accounts.add(p);
    }

    public Account getAccount(String email) {
        for (Account account : accounts) {
            if (account.getEmail().equalsIgnoreCase(email)) return account;
        }
        return null;
    }

    public ArrayList<Account> getAll() {
        return accounts;
    }

    public void addDummyAccount(Account account) {
        accounts.add(account);
    }

    public String findAll() {
        List<Account> accounts = (List<Account>) repository.findAll();
        String accounts_json = new Gson().toJson(accounts);
        System.out.println(accounts_json);
        return accounts_json;
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public void addAccount(Account account) {
        repository.save(account);
    }

    public String getAccByEmail(String email) {
        Account account = repository.findByEmail(email);
        String acc_json = new Gson().toJson(account);
        return acc_json;
    }







}
