package BurgerQueen.Favorites;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan({"FavoritesController","FavoritesService"})

public class FavoritesApplication {

//    public static void main(String[] args) {
//        SpringApplication.run(FavoritesApplication.class, args);
//    }

}
