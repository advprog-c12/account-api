package AccountModel;

import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "Account")
public class Account{

    @Id
    @Column(name = "acc_email")
    String email;

    @Column(name = "acc_name")
    String name;

    @Column(name = "acc_address")
    String address;

    public Account(){}

    public Account(String email){
        this.email=email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}